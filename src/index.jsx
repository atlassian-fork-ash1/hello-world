import ForgeUI, { render, Fragment, Macro, Text } from "@forge/ui";

const App = () => {
  return (
    <Fragment>
      <Text content="Hello world!" />
      <Text content="The content of your app will display here." />
    </Fragment>
  );
};

export const run = render(<Macro app={<App />} />);
