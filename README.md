# Forge Hello World

This project contains a Forge app written in Javascript that displays `Hello World!` in a Confluence macro. 

See [developer.atlassian.com/platform/forge/](https://developer.atlassian.com/platform/forge) for documentation and tutorials
explaining Forge.

## Requirements

You need the following:

- [Node.js](https://nodejs.org/en/download/) (version `12.12` or later)
- [Python](https://www.python.org/downloads/) (version 2.6.0 or later). The CLI is not compatible with Python 3 and requires that _python_ is present on 
your path.
- [Libsecret](https://wiki.gnome.org/Projects/Libsecret). Only required on Linux.
- Forge CLI (install by running `npm install -g @forge/cli`)

See [Set up Forge](https://developer.atlassian.com/platform/forge/set-up-forge/) for instructions to get set up.

## Quick start

- Install dependencies by running:
   ```
   npm install
   ```

- Modify your app by editing the `index.tsx` file.

- Build and deploy your app by running:
  ```
  forge deploy
  ```

- Install your app in an Atlassian site by running:
   ```
   forge install
   ```

### Notes
- Deploy your app, with the `forge deploy` command, any time you make changes to the code. 
- Install your app, with the `forge install` command, when you want to install your app on a new site. Once the app is 
installed on a site, the site picks up the new app changes you deploy without needing to run the install command again.

## Support

See [Get help](https://developer.atlassian.com/platform/forge/get-help/) for how to get help and provide feedback.
